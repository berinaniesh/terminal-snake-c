#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <signal.h>

#define COLS 60
#define ROWS 30

volatile bool STOP = false;
void sigint_handler(int sig);

void sigint_handler(int sig) {
    printf("\nCTRL-C detected\n");
    STOP = true;
}

int main() {
    signal(SIGINT, sigint_handler); // To detect sigint (ctrl+c) and do cleanup before exiting
    printf("\e[?25l"); // Hides the cursor   
    int quit = 0;
    while (!quit) {
	//Render the play surface
	printf("┌"); // Box left upper corner
	for (int i=0; i<COLS; i++) printf("─"); // Box top row
	printf("┐\n"); // Box right upper corner end
	for (int i=0; i<ROWS; i++) {
	    printf("│"); // Box left coloumn
	    for (int j=0; j<COLS; j++) printf("·"); // Inner dots
	    printf("│\n"); // Box right coloumn
	}
	printf("└"); // Box left down corner
	for (int i=0; i<COLS; i++) printf("─"); // Box bottom row
	printf("┘\n"); // Box right down corner
	printf("\e[%iA", ROWS+2);

	if (STOP==true) break; // STOP is set to true if signint is received. 
    }
    printf("\e[1;1H\e[2J"); // Clean terminal
    printf("\e[?25h"); // Show cursor again
    return 0;
}
